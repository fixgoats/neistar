## Uppsetning
Notaðu Python útgáfu sem Wagtail 2.15 styður.
Settu upp virtualenv í rót verkefnisins og ræstu það.
```
python -m venv env
source env/bin/activate
```
Settu upp dependencyin í `requirements-dev.txt`
```
pip install -r requirements-dev.txt
```
Settu upp Postgres gagnagrunn með notanda fixgoats (sjá `neistar/settings/dev.py`).
Þú getur líka bara sett hann upp eins og þú vilt svo lengi sem þú setur stillingarnar aftur eins og þær voru áður en þú gerir merge request.
Ef til vill væri hægt að setja þetta upp betur fyrir teymisvinnu í framtíðinni.

Til að keyra verkefnið keyrirðu einfaldlega
```
python manage.py runserver
```
og ef það koma upp einhver villuskilaboð fylgirðu leiðbeiningunum sem fylgja með þeim, yfirleitt er það bara að migratea gagnagrunninn.
