# Generated by Django 3.1 on 2020-10-17 01:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0052_pagelogentry'),
        ('base', '0004_auto_20201017_0125'),
    ]

    operations = [
        migrations.CreateModel(
            name='PennaIndex',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
            ],
            options={
                'verbose_name': 'höfundavísir',
            },
            bases=('wagtailcore.page',),
        ),
    ]
