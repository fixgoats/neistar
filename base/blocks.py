from wagtail.images.blocks import ImageChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.core.blocks import (
    CharBlock, ChoiceBlock, RichTextBlock, StreamBlock, StructBlock, TextBlock, FieldBlock,
)

from django import forms

class PullQuoteBlock(StructBlock):
    # Til að setja upp tilvísanir
    quote = TextBlock(label="Tilvísun")
    eftir = CharBlock(required=False)

    class Meta:
        icon = "openquote"

class ImageFormatChoiceBlock(FieldBlock):
    # Til að velja hvernig mynd er sett upp. fyrri orðin í orðapörunum eru notuð í CSS til uppsetningar.
    field = forms.ChoiceField(choices=(
        ("left", "Vinstri, vefja texta"), ("right", "Hægri, vefja texta"), ("mid", "Í miðju"), ("full", "Teygja yfir breidd greinar"),
    ))

class ImageBlock(StructBlock):
    # Til að setja inn myndir. "alt" er lýsingartextinn fyrir skjálesara og fólk sem hleður ekki niður myndunum.
    mynd = ImageChooserBlock()
    alt = CharBlock(label="Lýsing")
    texti = CharBlock(required=False)
    alignment = ImageFormatChoiceBlock(label="Snið")

class NeistarStreamBlock(StreamBlock):
    # Setur allar hinar blokkirnar saman í eina.
    inngangur = TextBlock(icon="title")
    undirkafli = TextBlock(icon="title")
    efnisgrein = RichTextBlock(
        icon="pilcrow",
        features = [
            "bold", "italic", "ol", "ul", "hr", "link", "document-link", "embed", "superscript", "subscript",
        ]
    )
    mynd = ImageBlock(icon="image")
    quote = PullQuoteBlock(label="Tilvísun")

#class ImageBlock(StructBlock):
#    """
#    Custom `StructBlock` for utilizing images with associated caption and
#    attribution data
#    """
#    image = ImageChooserBlock(required=True)
#    caption = CharBlock(required=False)
#    attribution = CharBlock(required=False)
#
#    class Meta:
#        icon = 'image'
#        template = "blocks/image_block.html"
#
#
#class HeadingBlock(StructBlock):
#    """
#    Custom `StructBlock` that allows the user to select h2 - h4 sizes for headers
#    """
#    heading_text = CharBlock(classname="title", required=True)
#    size = ChoiceBlock(choices=[
#        ('', 'Select a header size'),
#        ('h2', 'H2'),
#        ('h3', 'H3'),
#        ('h4', 'H4')
#    ], blank=True, required=False)
#
#    class Meta:
#        icon = "title"
#        template = "blocks/heading_block.html"
#
#
#class BlockQuote(StructBlock):
#    """
#    Custom `StructBlock` that allows the user to attribute a quote to the author
#    """
#    text = TextBlock()
#    attribute_name = CharBlock(
#        blank=True, required=False, label='e.g. Mary Berry')
#
#    class Meta:
#        icon = "fa-quote-left"
#        template = "blocks/blockquote.html"
#
#
## StreamBlocks
#class BaseStreamBlock(StreamBlock):
#    """
#    Define the custom blocks that `StreamField` will utilize
#    """
#    heading_block = HeadingBlock()
#    paragraph_block = RichTextBlock(
#        icon="fa-paragraph",
#        template="blocks/paragraph_block.html"
#    )
#    image_block = ImageBlock()
#    block_quote = BlockQuote()
#    embed_block = EmbedBlock(
#        help_text='Insert an embed URL e.g https://www.youtube.com/embed/SGJFWirQ3ks',
#        icon="fa-s15",
#        template="blocks/embed_block.html")
