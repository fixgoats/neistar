from __future__ import absolute_import, unicode_literals

from django.db import models
from django.utils import timezone
from django.http import HttpResponse
from django import forms
from django.shortcuts import render
from django.contrib.postgres.search import SearchVectorField

from wagtail.search import index
from wagtail.core import blocks, hooks
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import StreamField, RichTextField
from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, MultiFieldPanel, InlinePanel, PageChooserPanel, StreamFieldPanel
import wagtail.admin.rich_text.editors.draftail.features as draftail_features
from wagtail.admin.rich_text.converters.html_to_contentstate import InlineStyleElementHandler
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core.blocks import TextBlock, StructBlock, StreamBlock, FieldBlock, CharBlock, RichTextBlock, RawHTMLBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel
from wagtail.search import index
from wagtail.snippets.models import register_snippet
from wagtail.search.models import Query

from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

# def search(request):
#     # Search
#     search_query = request.GET.get("query", None)
#     if search_query:
#         search_results = Page.objects.live().descendant_of(request.site.root_page).search(search_query)

#         # Log the query so Wagtail can suggest promoted results
#         Query.get(search_query).add_hit()
#     else:
#         search_results = Page.objects.none()

#     # Render template
#     return render(request, "search_results.html", {
#         "search_query": search_query,
#         "search_results": search_results,
#     })

# Bætir við fídusum í ritilinn sem eru ekki til staðar annarsvegar
@hooks.register("register_rich_text_features")
def register_superscript_feature(features):
    feature_name = "superscript"
    type_ = "SUPERSCRIPT"
    tag = "sup"

    control = {
        "type": type_,
        "label": "sup",
        "description": "Yfirskrift, ",
    }

    features.register_editor_plugin(
        "draftail", feature_name, draftail_features.InlineStyleFeature(control)
    )

    db_conversion = {
        "from_database_format": {tag: InlineStyleElementHandler(type_)},
        "to_database_format": {"style_map": {type_: tag}},
    }

    features.register_converter_rule("contentstate", feature_name, db_conversion)

@hooks.register("register_rich_text_features")
def register_subscript_feature(features):
    feature_name = "subscript"
    type_ = "SUBSCRIPT"
    tag = "sub"

    control = {
        "type": type_,
        "label": "sub",
        "description": "Niðurskrift, ",
    }

    features.register_editor_plugin(
        "draftail", feature_name, draftail_features.InlineStyleFeature(control)
    )

    db_conversion = {
        "from_database_format": {tag: InlineStyleElementHandler(type_)},
        "to_database_format": {"style_map": {type_: tag}},
    }

    features.register_converter_rule("contentstate", feature_name, db_conversion)


# Blokkir fyrir EditorStreamBlock

class PullQuoteBlock(StructBlock):
    # Til að setja upp tilvísanir
    quote = TextBlock(label="Tilvísun")
    eftir = CharBlock(required=False)

    class Meta:
        icon = "openquote"

class ImageFormatChoiceBlock(FieldBlock):
    # Til að velja hvernig mynd er sett upp. fyrri orðin í orðapörunum eru notuð í CSS til uppsetningar.
    field = forms.ChoiceField(choices=(
        ("left", "Vinstri, vefja texta"), ("right", "Hægri, vefja texta"), ("mid", "Í miðju"), ("full", "Teygja yfir breidd greinar"),
    ))

class ImageBlock(StructBlock):
    # Til að setja inn myndir. "alt" er lýsingartextinn fyrir skjálesara og fólk sem hleður ekki niður myndunum.
    mynd = ImageChooserBlock()
    alt = CharBlock(label="Lýsing")
    texti = CharBlock(required=False)
    alignment = ImageFormatChoiceBlock(label="Snið")

class EditorStreamBlock(StreamBlock):
    # Setur allar hinar blokkirnar saman í eina.
    inngangur = TextBlock(icon="title")
    undirkafli = TextBlock(icon="title")
    efnisgrein = RichTextBlock(
        icon="pilcrow",
        features = [
            "bold", "italic", "ol", "ul", "hr", "link", "document-link", "embed", "superscript", "subscript",
        ]
    )
    mynd = ImageBlock(icon="image")
    quote = PullQuoteBlock(label="Tilvísun")

# Síðutegundir

class FormField(AbstractFormField):
    # Notað til að búa til eyður sem fólk á að fylla út í FormPage.
    page = ParentalKey("FormPage", related_name="form_fields")

class FormPage(AbstractEmailForm):
    # Notað fyrir "Um Okkur" síðuna. Gæti verið notað í framtíðinni fyrir aðrar síður þar sem við viljum fólk fylli inn eyðublað.
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FormSubmissionsPanel(),
        FieldPanel("intro", classname="full"),
        InlinePanel("form_fields", label="Form fields"),
        FieldPanel("thank_you_text", classname="full"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel("from_address", classname="col6"),
                FieldPanel("to_address", classname="col6"),
            ]),
            FieldPanel("subject"),
        ], "Email"),
    ]


class GreinarTag(TaggedItemBase):
    # Skilgreinir tögg fyrir greinar
    content_object = ParentalKey("grein", related_name="tagged_items")

class TagIndexPage(Page):
    # Notað til að búa til síðu fyrir hvert tagg á grein.

    def get_context(self, request):

        # Filter by tag
        tag = request.GET.get("tag")
        greinar = Grein.objects.filter(tags__name=tag).live().descendant_of(request.site.root_page).order_by("-first_published_at")

        # Update template context
        context = super(TagIndexPage, self).get_context(request)
        context["greinar"] = greinar
        return context

class AuthorProfile(Page):
    # Notað til að búa til höfundasíður. Löng saga hvernig þetta endaði á að heita *New*AuthorProfile en út af einhverjum ástæðum fríkar production gagnagrunnurinn út ef maður breytir nafninu svo svona verður þetta að vera í bili.
    efni = StreamField(EditorStreamBlock())
    nafn = models.CharField(max_length=255, verbose_name="nafn í þolfalli")
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name="þumalnögl",
    )

    content_panels = Page.content_panels + [
        FieldPanel("nafn"),
        StreamFieldPanel("efni"),
    ]

    promote_panels = Page.promote_panels + [
            ImageChooserPanel("thumbnail"),
        ]

    def get_context(self, request):
        context = super(AuthorProfile, self).get_context(request)
        greinar = Grein.objects.filter(penni=self).live().descendant_of(request.site.root_page).order_by("-first_published_at")
        context["greinar"] = greinar
        return context

    class Meta:
        verbose_name = "höfundarsíða"

class Grein(Page):
    # Notað til að búa til greinar
    brennidepill = models.BooleanField()
    penni = models.CharField(max_length=255)
    penni_url = models.URLField("Síða höfundar", blank=True, null=True)
    dagsetning = models.DateField()
    efni = StreamField(EditorStreamBlock())
    tags = ClusterTaggableManager("tögg", through=GreinarTag, blank=True)
    kynning = models.TextField(blank=True, null=True)
    flokkur = models.ForeignKey("Flokkur", blank=True, null=True, on_delete=models.SET_NULL)
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name="þumalnögl",
    )
    #tengdar_greinar = models.ForeignKey(
    #            "wagtailcore.Page",
    #            null=True,
    #            blank=True,
    #            on_delete=models.SET_NULL,
    #            related_name="+",
    #        )

    search_fields = Page.search_fields + [
        index.SearchField("efni"),
        index.FilterField("dagsetning"),
        index.FilterField("penni"),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel("penni"),
            FieldPanel("penni_url"),
            FieldPanel("dagsetning"),
            FieldPanel("flokkur", widget=forms.Select),
        ], heading="Upplýsingar um greinina"),
        FieldPanel("kynning"),
        StreamFieldPanel("efni"),
        #PageChooserPanel("tengdar_greinar"),
    ]
    promote_panels = Page.promote_panels + [
        ImageChooserPanel("thumbnail"),
        FieldPanel("tags"),
        FieldPanel("brennidepill"),
    ]

class SimplePage(Page):
    efni = StreamField(EditorStreamBlock(), blank=True)
    dagsetning = models.DateField(blank=True, null=True)
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name="þumalnögl",
    )
    
    content_panels = Page.content_panels + [
            FieldPanel("dagsetning"),
            StreamFieldPanel("efni"),
        ]

    promote_panels = Page.promote_panels + [
            ImageChooserPanel("thumbnail"),
        ]

    search_fields = Page.search_fields + [
            index.SearchField("efni"),
        ]

    class Meta:
        verbose_name = "einföld grein (hugsað fyrir heimasíðuna)"

@register_snippet
class Flokkur(models.Model):
    # Notað til að skilgreina flokka sem greinar og flokkasíður þurfa að falla í.
    nafn = models.CharField(max_length=255)

    panels = [
        FieldPanel("nafn"),
    ]

    def __str__(self):
        return self.nafn

    class Meta:
        verbose_name_plural = "flokkar"

class FlokkurIndex(Page):
    # Notað til búa til flokkasíður sem safna saman öllum greinum úr ákveðnum flokkum.
    flokkur = models.ForeignKey("Flokkur", null=True, on_delete=models.SET_NULL)

    content_panels = Page.content_panels + [FieldPanel("flokkur")]

    def get_context(self, request):
        context = super(FlokkurIndex, self).get_context(request)
        greinar = Grein.objects.filter(flokkur=self.flokkur).live().descendant_of(request.site.root_page).order_by("-first_published_at")
        context["greinar"] = greinar
        return context

    class Meta:
        verbose_name = "flokksvísir"

class SimpleIndex(Page):
    def get_context(self, request):
        context = super(SimpleIndex, self).get_context(request)
        pages = Page.objects.child_of(self).order_by("-first_published_at")
        context["pages"] = pages
        return context

    class Meta:
        verbose_name = "einfaldur vísir"

class HomePage(Page):
    # Notað til að búa til heimasíðuna sem sýnir allar greinar í u.þ.b. réttri öfugri tímaröð (röðin er ekki alveg rétt því CSS-ið getur ruglað pínu í röðinni)
    def get_context(self, request):
        context = super(HomePage, self).get_context(request)
        greinar = Grein.objects.live().descendant_of(self).order_by("-first_published_at")
        context["greinar"] = greinar
        return context

    class Meta:
        verbose_name = "heimasíða"
