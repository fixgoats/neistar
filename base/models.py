from __future__ import unicode_literals

from django.db import models
#from django.http import HttpResponse
from django import forms
#from django.shortcuts import render

from modelcluster.models import ClusterableModel
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase

from wagtail.search import index
from wagtail.core import blocks, hooks
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import StreamField, RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel
from wagtail.snippets.models import register_snippet
from wagtail.admin.edit_handlers import (
    FieldPanel,
    FieldRowPanel,
    MultiFieldPanel,
    InlinePanel,
    PageChooserPanel,
    StreamFieldPanel,
)

from django.template.defaulttags import register

from .blocks import NeistarStreamBlock

CONTACT_TYPES = [
    ('EMAIL', 'Tölvupóstur'),
    ('MASTODON', 'Mastodon'),
    ('TWITTER', 'Twitter'),
    ('FB', 'Facebook'),
]

class FrontPage(Page):
    """
    Notað til að búa til forsíðuna sem sýnir allar greinar í u.þ.b. réttri öfugri tímaröð
    """
    def get_context(self, request):
        context = super(FrontPage, self).get_context(request)
        greinar = Grein.objects.live().descendant_of(self).order_by("-dagsetning")
        menu_items = self.get_children().live().in_menu()
        context["greinar"] = greinar
        context["menu_items"] = menu_items
        return context

    class Meta:
        verbose_name = "forsíða"

class PennaIndex(Page):
    content_panels = Page.content_panels

    def get_context(self, request):
        context = super().get_context(request)
        pennar = PennaProfile.objects.live().order_by("-first_published_at")
        context["pennar"] = pennar
        return context

    class Meta:
        verbose_name = "höfundavísir"

class GreinarMerki(TaggedItemBase):
    """
    Merki fyrir greinar, nánari flokkun en "flokkar"
    """
    content_object = ParentalKey("grein", related_name="tagged_items", on_delete=models.CASCADE)

class MerkjaIndex(Page):
    """
    Notað til að búa til síðu fyrir hvert merki á grein.
    """

    def get_context(self, request):

        # Filter by tag
        merki = request.GET.get("merki")
        greinar = Grein.objects.filter(merki__name=merki).live().order_by("-first_published_at")

        # Update template context
        context = super(MerkjaIndex, self).get_context(request)
        context["greinar"] = greinar
        return context

class PennaContactInfo(Orderable):
    penni = ParentalKey("PennaProfile", on_delete=models.CASCADE, related_name="contact_info")
    site = models.CharField(max_length=255, choices=CONTACT_TYPES, verbose_name="Síða/miðill")
    handle = models.CharField(max_length=255, verbose_name="Póstfang/notandanafn")

    panels= [
        FieldPanel("penni"),
        FieldPanel("site"),
        FieldPanel("handle"),
    ]

    @register.filter
    def parse_mastodon(self):
        if self.site == "MASTODON":
            return self.handle.split("@")

class PennaProfile(Page):
    """
    Síða fyrir höfunda. Hér getur höfundur skrifað eitthvað um sjálfan sig og sett sambands- og samfélagsmiðlaupplýsingar ef vill.
    """
    nafn = models.CharField(max_length=255, verbose_name="nafn í þolfalli")
    efni = StreamField(NeistarStreamBlock(required=False), blank=True, null=True)
    mynd = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name="mynd af höfundi (vinsamlegast með upplausn nálægt 240x300)",
    )

    content_panels = Page.content_panels + [
        FieldPanel("nafn"),
        ImageChooserPanel("mynd"),
        StreamFieldPanel("efni"),
        InlinePanel("contact_info", label="Sambandsupplýsingar"),
    ]

    @register.filter
    def site_by_name(self, site):
        for item in self.contact_info.all():
            if item.site == site:
                return item

    def get_context(self, request):
        context = super().get_context(request)
        greinar = Grein.objects.filter(pennar__penni=self).live().order_by("-dagsetning")
        context["greinar"] = greinar
        return context

    class Meta:
        verbose_name = "höfundarsíða"

class GreinPenni(Orderable):
    grein = ParentalKey("Grein", on_delete=models.CASCADE, related_name="pennar")
    penni = models.ForeignKey(PennaProfile, on_delete=models.CASCADE)

    panels = [
            FieldPanel("grein"),
            FieldPanel("penni"),
    ]

    def title(self):
        return self.penni.title
    def url(self):
        return self.penni.url

class GreinTengdGrein(Orderable):
    grein = ParentalKey("Grein", on_delete=models.CASCADE, related_name="tengdar_greinar")
    tengd_grein = models.ForeignKey("Grein", on_delete=models.CASCADE)

    panels = [
            FieldPanel("grein"),
            FieldPanel("tengd_grein"),
    ]

    def title(self):
        return self.tengd_grein.title

class Grein(Page):
    """
    Notað til að skrifa greinar
    """
    brennidepill = models.BooleanField()
    dagsetning = models.DateField()
    efni = StreamField(NeistarStreamBlock())
    kynning = models.TextField(max_length=500)
    flokkur = models.ForeignKey("Flokkur", null=True, on_delete=models.SET_NULL)
    merki = ClusterTaggableManager(through=GreinarMerki, blank=True)
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name="þumalnögl",
    )

    search_fields = Page.search_fields + [
        index.SearchField("efni"),
        index.SearchField("dagsetning"),
        index.SearchField("kynning"),
        index.RelatedFields("pennar", [
            index.SearchField("title"),
        ]),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            InlinePanel("pennar", label="Höfundar"),
            FieldPanel("dagsetning"),
            FieldPanel("flokkur", widget=forms.Select),
            FieldPanel("kynning"),
        ], heading="Upplýsingar um greinina"),
        StreamFieldPanel("efni"),
    ]

    promote_panels = Page.promote_panels + [
        ImageChooserPanel("thumbnail"),
        FieldPanel("merki"),
        FieldPanel("brennidepill"),
        InlinePanel("tengdar_greinar", label="Tengdar greinar"),
    ]

@register_snippet
class Flokkur(models.Model):
    """
    Notað til að skilgreina flokka fyrir greinar
    """
    nafn = models.CharField(max_length=255)
    panels = [FieldPanel("nafn")]
    def __str__(self):
        return self.nafn
    class Meta:
        verbose_name_plural = "flokkar"

class FlokkurIndex(Page):
    """
    Flokksvísir til að safna saman greinum af ákveðnum flokki
    """
    flokkur = models.ForeignKey("Flokkur", on_delete=models.PROTECT)

    content_panels = Page.content_panels + [FieldPanel("flokkur")]

    def get_context(self, request):
        context = super().get_context(request)
        greinar = Grein.objects.filter(flokkur=self.flokkur).live().order_by("-dagsetning")
        context["greinar"] = greinar
        return context

    class Meta:
        verbose_name = "flokksvísir"

class GreinaIndex(Page):

    def get_context(self, request):
        context = super().get_context(request)
        greinar = Grein.objects.live().order_by("-dagsetning")
        context["greinar"] = greinar
        return context

class FormField(AbstractFormField):
    """
    Notað til að búa til eyður sem fólk á að fylla út í FormPage.
    """
    page = ParentalKey("FormPage", related_name="form_fields")

class FormPage(AbstractEmailForm):
    """
    Notað fyrir "Um Okkur" síðuna. Gæti verið notað í framtíðinni fyrir aðrar síður þar sem við viljum fólk fylli inn eyðublað.
    """
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FormSubmissionsPanel(),
        FieldPanel("intro", classname="full"),
        InlinePanel("form_fields", label="Form fields"),
        FieldPanel("thank_you_text", classname="full"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel("from_address", classname="col6"),
                FieldPanel("to_address", classname="col6"),
            ]),
            FieldPanel("subject"),
        ], "Email"),
    ]
