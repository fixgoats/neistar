from django import template

from ..models import Valstika

register = template.Library()

@register.simple_tag()
def get_menu(nafn):
    return Valstika.objects.filter(titill=nafn).first()
