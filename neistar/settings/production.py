from .base import *

DEBUG = False
ALLOWED_HOSTS = ['*']

SECRET_KEY = os.environ['DJANGO_SECRET_KEY']
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = os.environ['NEISTAR_EMAIL_HOST']
EMAIL_PORT = '587'
EMAIL_HOST_USER = os.environ['NEISTAR_EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = os.environ['NEISTAR_EMAIL_HOST_PASSWORD']
EMAIL_USE_TLS = True
EMAIL_TIMEOUT = 30

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'neistardb',
        'USER': 'neistardbuser',
        'PASSWORD': os.environ['NEISTARDB_PASS'],
    }
}

MEDIA_ROOT = '/srv/www/neistar-media/media'
MEDIA_URL = '/media/'

try:
    from .local import *
except ImportError:
    pass
