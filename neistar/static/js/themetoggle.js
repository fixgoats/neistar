const dlbutton = document.querySelector('#darklightmodebutton');

dlbutton.onclick = function() {
    const darkcss = document.querySelector("#darkcss");
    const lightcss = document.querySelector("#lightcss");

    theme = localStorage.getItem('theme') || '';
    if (theme === '') {
        darkcss.disabled = false;
        lightcss.disabled = true;
        localStorage.setItem('theme', 'dark');
    }
    else if (theme === 'dark') {
        darkcss.disabled = true;
        lightcss.disabled = false;
        localStorage.setItem('theme', '');
    }
}
