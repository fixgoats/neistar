const menutoggle = document.querySelector('#menutoggle');
const menu = document.querySelector('#mainstika');

menutoggle.onclick = function() {
    if (menu.style.display != 'block') {
        menu.style.display = 'block';
    }

    else if (menu.style.display != 'none') {
        menu.style.display = 'none';
    }
}

window.addEventListener('click', event => {
    if (event.target != menu && !menu.contains(event.target) && event.target != menutoggle) {
        menu.style.display = 'none';
    }
})

document.onkeyup = function(event) {
    if (event.key === 'Escape') {
        menu.style.display = 'none';
    }
}
