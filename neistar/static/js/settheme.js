(function() {
    const darkcss = document.querySelector("#darkcss");
    const lightcss = document.querySelector("#lightcss");

    let theme = localStorage.getItem('theme') || '';
    if (theme === 'dark') {
        darkcss.disabled = false;
        lightcss.disabled = true;
    }
})();
